import 'dart:async';
import 'dart:convert';

import 'package:flutter/services.dart';

import 'avimmessage.dart';
import 'avimconversation.dart';

export 'avimmessage.dart';
export 'avimconversation.dart';

typedef UnreadMessagesCountUpdatedCallback = void Function(AVIMConversation);
typedef ReceiveMessageCallback = void Function(AVIMMessage, AVIMConversation);
typedef AVIMClientStatusChangeCallback = void Function(String?);

class LeancloudIm {
  static const MethodChannel _channel = const MethodChannel('leancloud_im');

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static LeancloudIm? _instance;

  static LeancloudIm? get shared {
    if (_instance == null) {
      _instance = LeancloudIm();
      _channel.setMethodCallHandler(_instance!.handleMethodCall);
    }
    return _instance;
  }

  Future<dynamic> handleMethodCall(MethodCall call) async {
    switch (call.method) {
      case "unreadMessagesCountUpdated":
        if (_unreadMessagesCountUpdatedCallback != null) {
          String conversationJson = (call.arguments)["conversation"];
          AVIMConversation conversation =
              AVIMConversation.fromJson(json.decode(conversationJson));
          _unreadMessagesCountUpdatedCallback!(conversation);
        }
        return new Future.value(true);
        break;
      case "receiveMessage":
        if (_receiveMessageCallback != null) {
          String conversationJson = (call.arguments)["conversation"];
          AVIMConversation conversation =
              AVIMConversation.fromJson(json.decode(conversationJson));
          String messageJson = (call.arguments)["message"];
          AVIMMessage message = AVIMMessage.fromJson(json.decode(messageJson));

          _receiveMessageCallback!(message, conversation);
        }
        return new Future.value(true);
        break;
      case "onClientStatusChanged":
        if (_avImClientStatusCallback != null) {
          String? status = (call.arguments)["status"];
          _avImClientStatusCallback!(status);
        }
        return new Future.value(true);
        break;
      default:
        break;
    }
    return Future.value(true);
  }

  UnreadMessagesCountUpdatedCallback? _unreadMessagesCountUpdatedCallback;
  void addUnreadMessagesCountUpdatedListener(
      UnreadMessagesCountUpdatedCallback callback) {
    _unreadMessagesCountUpdatedCallback = callback;
  }

  ReceiveMessageCallback? _receiveMessageCallback;
  void addReceiveMessageListener(ReceiveMessageCallback callback) {
    _receiveMessageCallback = callback;
  }

  /// Status
  /// offline
  /// paused
  /// resume
  AVIMClientStatusChangeCallback? _avImClientStatusCallback;
  void addAVIMClientStatusChangeListener(
      AVIMClientStatusChangeCallback callback) {
    _avImClientStatusCallback = callback;
  }

  void setApplicationId(String appId, String appKey) {
    var args = <String, dynamic>{
      'appId': appId,
      'appKey': appKey,
    };

    _channel.invokeMethod("setApplicationId", args);
  }

  Future<Null> setClient(String clientId) async {
    var args = <String, dynamic>{'clientId': clientId};

    await _channel.invokeMethod("setClient", args);
  }

  Future<bool?> open() async {
    bool? isSucceed = await _channel.invokeMethod("open");
    return isSucceed;
  }

  Future<bool?> close() async {
    bool? isSucceed = await _channel.invokeMethod("close");
    return isSucceed;
  }

  Future<AVIMConversation> createConversation(
      String name, List<String> clientIds) async {
    var args = <String, dynamic>{
      'name': name,
      'clientIds': clientIds,
    };
    String conversationJson =
        (await (_channel.invokeMethod("createConversation", args))) as String;

    return AVIMConversation.fromJson(json.decode(conversationJson));
  }

  Future<bool?> sendMessage(String conversationId, AVIMMessage message) async {
    // a dirty support for video call
    if (message.mediaType == kAVIMMessageMediaTypeVideoCall) {
      message.mediaType = kAVIMMessageMediaTypeText;
      message.payload = "@eacall@${message.payload}";
    }
    var args = <String, dynamic>{
      'conversationId': conversationId,
      'message': json.encode(message.toJson()),
    };
    return await _channel.invokeMethod("sendMessage", args);
  }

  Future<List<AVIMConversation>> queryConversationList([int limit = 10]) async {
    var args = <String, dynamic>{'limit': limit};
    List<dynamic> conversationsJson = (await (_channel.invokeMethod(
        "queryConversationList", args))) as List<dynamic>;
    List<AVIMConversation> conversations = conversationsJson
        .map(((conversationJson) =>
            AVIMConversation.fromJson(json.decode(conversationJson))))
        .toList();
    return conversations;
  }

  void conversationRead(String conversationId) {
    var args = <String, dynamic>{
      'conversationId': conversationId,
    };

    _channel.invokeMethod("conversationRead", args);
  }

  Future<List<AVIMMessage>> queryMessages(String conversationId,
      [int limit = 10]) async {
    var args = <String, dynamic>{
      'limit': limit,
      'conversationId': conversationId
    };
    List<dynamic> messagesJson =
        (await (_channel.invokeMethod("queryMessages", args))) as List<dynamic>;
    List<AVIMMessage> messages = messagesJson
        .map(((messageJson) => AVIMMessage.fromJson(json.decode(messageJson))))
        .toList();
    return messages;
  }

  Future<bool?> updateConversationName(
      String conversationId, String name) async {
    var args = <String, dynamic>{
      'conversationId': conversationId,
      'name': name,
    };

    return await _channel.invokeMethod("updateConversationName", args);
  }
}
