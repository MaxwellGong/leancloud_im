// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'avimmessage.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AVIMMessage _$AVIMMessageFromJson(Map<String, dynamic> json) => AVIMMessage(
      json['status'] as int?,
      json['conversationId'] as String?,
      json['timestamp'] as int?,
      json['receiptTimestamp'] as int?,
      json['clientId'] as String?,
      json['content'] as String?,
      json['mediaType'] as int?,
      json['messageId'] as String?,
      json['payload'] as String?,
      json['ioType'] as int?,
    );

Map<String, dynamic> _$AVIMMessageToJson(AVIMMessage instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('status', instance.status);
  writeNotNull('conversationId', instance.conversationId);
  writeNotNull('timestamp', instance.timestamp);
  writeNotNull('receiptTimestamp', instance.receiptTimestamp);
  writeNotNull('clientId', instance.clientId);
  writeNotNull('content', instance.content);
  writeNotNull('mediaType', instance.mediaType);
  writeNotNull('messageId', instance.messageId);
  writeNotNull('payload', instance.payload);
  writeNotNull('ioType', instance.ioType);
  return val;
}
