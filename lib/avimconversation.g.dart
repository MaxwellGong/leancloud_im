// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'avimconversation.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AVIMConversation _$AVIMConversationFromJson(Map<String, dynamic> json) =>
    AVIMConversation(
      json['unreadMessagesCount'] as int?,
      json['name'] as String?,
      json['creator'] as String?,
      json['lastMessage'] == null
          ? null
          : AVIMMessage.fromJson(json['lastMessage'] as Map<String, dynamic>),
      json['conversationId'] as String?,
      (json['members'] as List<dynamic>?)?.map((e) => e as String).toList(),
      json['updatedAt'] as int?,
      json['lastMessageAt'] as int?,
    );

Map<String, dynamic> _$AVIMConversationToJson(AVIMConversation instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('unreadMessagesCount', instance.unreadMessagesCount);
  writeNotNull('name', instance.name);
  writeNotNull('creator', instance.creator);
  writeNotNull('lastMessage', instance.lastMessage);
  writeNotNull('conversationId', instance.conversationId);
  writeNotNull('members', instance.members);
  writeNotNull('updatedAt', instance.updatedAt);
  writeNotNull('lastMessageAt', instance.lastMessageAt);
  return val;
}
