// GENERATED CODE - DO NOT MODIFY BY HAND
// step: flutter packages pub run build_runner build --delete-conflicting-outputs

import 'dart:convert';
import 'package:json_annotation/json_annotation.dart';

part 'avimmessage.g.dart';

@JsonSerializable(includeIfNull: false)
class AVIMMessage {
  AVIMMessage(
    this.status,
    this.conversationId,
    this.timestamp,
    this.receiptTimestamp,
    this.clientId,
    this.content,
    this.mediaType,
    this.messageId,
    this.payload,
    this.ioType,
  );

  factory AVIMMessage.fromJson(Map<String, dynamic> json) =>
      _$AVIMMessageFromJson(json);
  Map<String, dynamic> toJson() => _$AVIMMessageToJson(this);

  int? status;
  String? conversationId;
  int? timestamp;
  int? receiptTimestamp;
  String? clientId;
  String? content;
  int? mediaType;
  String? messageId;
  String? payload;
  int? ioType;

  factory AVIMMessage.initial() {
    return AVIMMessage(
      0,
      null,
      0,
      0,
      null,
      null,
      0,
      null,
      null,
      1,
    );
  }

  factory AVIMMessage.copy(AVIMMessage model) {
    return AVIMMessage.fromJson(json.decode(json.encode(model.toJson())));
  }

  AVIMMessage copyWith({
    int? status,
    String? conversationId,
    int? timestamp,
    int? receiptTimestamp,
    String? clientId,
    String? content,
    int? meidaType,
    String? messageId,
    String? fileUrl,
    int? ioType,
  }) {
    return AVIMMessage(
      status ?? this.status,
      conversationId ?? this.conversationId,
      timestamp ?? this.timestamp,
      receiptTimestamp ?? this.receiptTimestamp,
      clientId ?? this.clientId,
      content ?? this.content,
      meidaType ?? this.mediaType,
      messageId ?? this.messageId,
      fileUrl ?? this.payload,
      ioType ?? this.ioType,
    );
  }

  @override
  bool operator ==(Object other) =>
      other is AVIMMessage &&
      runtimeType == other.runtimeType &&
      this.status == other.status &&
      this.conversationId == other.conversationId &&
      this.timestamp == other.timestamp &&
      this.receiptTimestamp == other.receiptTimestamp &&
      this.clientId == other.clientId &&
      this.content == other.content &&
      this.mediaType == other.mediaType &&
      this.messageId == other.messageId &&
      this.payload == other.payload &&
      this.ioType == other.ioType;

  @override
  int get hashCode =>
      this.status.hashCode ^
      this.conversationId.hashCode ^
      this.timestamp.hashCode ^
      this.receiptTimestamp.hashCode ^
      this.clientId.hashCode ^
      this.content.hashCode ^
      this.mediaType.hashCode ^
      this.messageId.hashCode ^
      this.payload.hashCode ^
      this.ioType.hashCode;
}

const AVIMMessageStatusNone = 0;
const AVIMMessageStatusSending = 1;
const AVIMMessageStatusSent = 2;
const AVIMMessageStatusDelivered = 3;
const AVIMMessageStatusFailed = 4;
const AVIMMessageStatusRead = 5;

const AVIMMessageIOTypeIn = 1;
const AVIMMessageIOTypeOut = 2;

const kAVIMMessageMediaTypeNone = 0;
const kAVIMMessageMediaTypeText = -1;
const kAVIMMessageMediaTypeImage = -2;
const kAVIMMessageMediaTypeAudio = -3;
const kAVIMMessageMediaTypeVideo = -4;
const kAVIMMessageMediaTypeLocation = -5;
const kAVIMMessageMediaTypeFile = -6;
const kAVIMMessageMediaTypeRecalled = -127;
const kAVIMMessageMediaTypeVideoCall = 2;
