// GENERATED CODE - DO NOT MODIFY BY HAND
// step: flutter packages pub run build_runner build --delete-conflicting-outputs

import 'dart:convert';
import 'package:json_annotation/json_annotation.dart';

import 'avimmessage.dart';

part 'avimconversation.g.dart';

@JsonSerializable(includeIfNull: false)
class AVIMConversation {
  AVIMConversation(
    this.unreadMessagesCount,
    this.name,
    this.creator,
    this.lastMessage,
    this.conversationId,
    this.members,
    this.updatedAt,
    this.lastMessageAt,
  );

  factory AVIMConversation.fromJson(Map<String, dynamic> json) =>
      _$AVIMConversationFromJson(json);
  Map<String, dynamic> toJson() => _$AVIMConversationToJson(this);

  int? unreadMessagesCount;
  String? name;
  String? creator;
  AVIMMessage? lastMessage;
  String? conversationId;
  List<String>? members;
  int? updatedAt;
  int? lastMessageAt;

  factory AVIMConversation.initial() {
    return AVIMConversation(
      null,
      null,
      null,
      AVIMMessage.initial(),
      null,
      [],
      null,
      null,
    );
  }

  factory AVIMConversation.copy(AVIMConversation model) {
    return AVIMConversation.fromJson(json.decode(json.encode(model.toJson())));
  }

  AVIMConversation copyWith({
    int? unreadMessagesCount,
    String? name,
    String? creator,
    AVIMMessage? lastMessage,
    String? conversationId,
    List<String>? members,
    int? updatedAt,
    int? lastMessageAt,
  }) {
    return AVIMConversation(
      unreadMessagesCount ?? this.unreadMessagesCount,
      name ?? this.name,
      creator ?? this.creator,
      lastMessage ?? this.lastMessage,
      conversationId ?? this.conversationId,
      members ?? this.members,
      updatedAt ?? this.updatedAt,
      lastMessageAt ?? this.lastMessageAt,
    );
  }

  @override
  bool operator ==(Object other) =>
      other is AVIMConversation &&
      runtimeType == other.runtimeType &&
      this.unreadMessagesCount == other.unreadMessagesCount &&
      this.name == other.name &&
      this.creator == other.creator &&
      this.lastMessage == other.lastMessage &&
      this.conversationId == other.conversationId &&
      this.members == other.members &&
      this.updatedAt == other.updatedAt &&
      this.lastMessageAt == other.lastMessageAt;

  @override
  int get hashCode =>
      this.unreadMessagesCount.hashCode ^
      this.name.hashCode ^
      this.creator.hashCode ^
      this.lastMessage.hashCode ^
      this.conversationId.hashCode ^
      this.members.hashCode ^
      this.updatedAt.hashCode ^
      this.lastMessageAt.hashCode;
}
