#import "JSONModel.h"
#import <AVOSCloudIM/AVOSCloudIM.h>

@interface MessageModel : JSONModel

@property (nonatomic) AVIMMessageStatus status;
@property (nonatomic, strong) NSString <Optional> * conversationId;
@property (nonatomic) NSInteger timestamp;
@property (nonatomic) NSInteger receiptTimestamp;
@property (nonatomic, strong) NSString <Optional> * clientId;
@property (nonatomic, strong) NSString <Optional> * content;
@property (nonatomic, strong) NSString <Optional> * messageId;
@property (nonatomic, strong) NSString <Optional> * payload;
@property (nonatomic) AVIMMessageIOType  ioType;
@property (nonatomic) NSInteger mediaType;

@end

