#import <JSONModel/JSONModel.h>
#import "MessageModel.h"

@interface ConversationModel : JSONModel 

@property (nonatomic) NSInteger unreadMessagesCount;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* creator;
@property (nonatomic, strong) MessageModel* lastMessage;
@property (nonatomic, strong) NSString* conversationId;
@property (nonatomic, strong) NSArray<NSString*>* members;
@property (nonatomic) NSInteger updatedAt;
@property (nonatomic) NSInteger lastMessageAt;

@end
