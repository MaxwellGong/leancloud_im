#import "LeancloudImPlugin.h"
#import <AVOSCloud/AVOSCloud.h>
#import "ConversationModel.h"

@implementation LeancloudImPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
    FlutterMethodChannel* channel = [FlutterMethodChannel
                                     methodChannelWithName:@"leancloud_im"
                                     binaryMessenger:[registrar messenger]];
    LeancloudImPlugin* instance = [LeancloudImPlugin shareInstance];
    LeancloudImPlugin.shareInstance.channel = channel;
    LeancloudImPlugin.shareInstance.cachedConversations = [NSMutableDictionary dictionary];
    [registrar addMethodCallDelegate:instance channel:channel];
}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
    if ([@"getPlatformVersion" isEqualToString:call.method]) {
        result([@"iOS " stringByAppendingString:[[UIDevice currentDevice] systemVersion]]);
    } else if ([@"setApplicationId" isEqualToString:call.method]) {
        NSString *appId = call.arguments[@"appId"];
        NSString *appKey = call.arguments[@"appKey"];
        [self setApplicationId: appId appKey:appKey];
    } else if ([@"setClient" isEqualToString:call.method]) {
        NSString *clientId = call.arguments[@"clientId"];
        [self initClient: clientId];
        result(@YES);
    } else if ([@"open" isEqualToString:call.method]) {
        [self openWithCallback:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                result(@YES);
            } else {
                result([FlutterError errorWithCode: @"leancloud_im" message: error.localizedDescription details: error.localizedFailureReason]);
            }
        }];
    } else if ([@"close" isEqualToString:call.method]) {
        [self closeWithCallback:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                result(@YES);
            } else {
                result([FlutterError errorWithCode: @"leancloud_im" message: error.localizedDescription details: error.localizedFailureReason]);
            }
        }];
    } else if ([@"createConversation" isEqualToString:call.method]) {
        NSString *name = call.arguments[@"name"];
        NSArray *clientIds = call.arguments[@"clientIds"];
        [self createConversationWithName: name clientIds:clientIds callback:^(AVIMConversation *conversation, NSError *error) {
            if (error == nil) {
                self->_cachedConversations[conversation.conversationId] = conversation;
                ConversationModel* model =  _convertConverstionToModel(conversation);
                result(model.toJSONString);
            } else {
                result([FlutterError errorWithCode: @"leancloud_im" message: error.localizedDescription details: error.localizedFailureReason]);
            }
        }];
    } else if ([@"sendMessage" isEqualToString:call.method]) {
        NSString *conversationId = call.arguments[@"conversationId"];
        NSString *message = call.arguments[@"message"];
        
        [self sendMessage:conversationId message:message callback:^(BOOL succeeded, NSError *error) {
            if (error == nil) {
                result(@YES);
            } else {
                result([FlutterError errorWithCode: @"leancloud_im" message: error.localizedDescription details: error.localizedFailureReason]);
            }
        }];
    } else if ([@"queryConversationList" isEqualToString:call.method]) {
        NSInteger limit = [call.arguments[@"limit"] integerValue];
        
        [self findConversationsWithCallback:limit callback:^(NSArray<AVIMConversation *>* objects, NSError * error) {
            if (error == nil) {
                NSMutableArray<NSString*>* conversationsJson = [NSMutableArray arrayWithCapacity:objects.count];
                for (AVIMConversation* object in objects) {
                    self->_cachedConversations[object.conversationId] = object;
                    ConversationModel* model = _convertConverstionToModel(object);
                    [conversationsJson addObject: model.toJSONString];
                }
                result([conversationsJson copy]);
            } else {
                result([FlutterError errorWithCode: @"leancloud_im" message: error.localizedDescription details: error.localizedFailureReason]);
            }
        }];
    } else if ([@"conversationRead" isEqualToString:call.method]) {
        NSString *conversationId = call.arguments[@"conversationId"];
        AVIMConversation* conversation = [self getConversationById: conversationId];
        [conversation readInBackground];
    } else if ([@"queryMessages" isEqualToString:call.method]) {
        NSInteger limit = [call.arguments[@"limit"] integerValue];
        NSString *conversationId = call.arguments[@"conversationId"];
        
        [self queryMessages:conversationId limit:limit callback:^(NSArray<AVIMMessage *> * _Nullable objects, NSError * _Nullable error) {
            if (error == nil) {
                NSMutableArray<NSString*>* messagesJson = [NSMutableArray arrayWithCapacity:objects.count];
                for (id object in objects) {
                    MessageModel* model = _convertMessageToModel(object);
                    [messagesJson addObject: model.toJSONString];
                }
                result([messagesJson copy]);
            } else {
                result([FlutterError errorWithCode: @"leancloud_im" message: error.localizedDescription details: error.localizedFailureReason]);
            }
        }];
    } else if ([@"updateConversationName" isEqualToString:call.method]) {
        NSString *conversationId = call.arguments[@"conversationId"];
        NSString *name = call.arguments[@"name"];
        [self updateConversationName:conversationId name:name callback:^(BOOL succeeded, NSError * error) {
            if (error == nil) {
                result(@YES);
            } else {
                result([FlutterError errorWithCode: @"leancloud_im" message: error.localizedDescription details: error.localizedFailureReason]);
            }
        }];
    }
    else {
        result(FlutterMethodNotImplemented);
    }
}



- (void) setApplicationId: (NSString*)appId appKey:(NSString*)appKey {
    if (self.client != nil && self.client.clientId != nil) {
        return;
    }
    [AVOSCloud setApplicationId:appId clientKey:appKey];
    [AVOSCloud setAllLogsEnabled:YES];
    [AVIMClient setUnreadNotificationEnabled:YES];
}

- (void) initClient: (NSString*)clientId {
    self.client = [[AVIMClient alloc] initWithClientId:clientId];
    self.client.delegate = self;
}

- (void) openWithCallback: (void (^)(BOOL, NSError * _Nullable))callback {
    [self.client openWithCallback:^(BOOL succeeded, NSError *error) {
        callback(succeeded, error);
    }];
}

- (void) closeWithCallback: (void (^)(BOOL, NSError * _Nullable))callback {
    [self.client closeWithCallback:^(BOOL succeeded, NSError *error) {
        callback(succeeded, error);
    }];
}

- (void) createConversationWithName: (NSString*)name clientIds:(NSArray<NSString *> *)clientIds callback:(void (^)(AVIMConversation*, NSError *_Nullable))callback {
    [self.client createConversationWithName:name clientIds:clientIds attributes:nil options:AVIMConversationOptionUnique callback:^(AVIMConversation *conversation, NSError *error) {
        callback(conversation, error);
    }];
}

- (void) sendMessage: (NSString*)conversationId message:(NSString*)message callback:(void (^)(BOOL, NSError * _Nullable))callback {
    AVIMConversation* conversation = [self getConversationById: conversationId];
    MessageModel* model = [[MessageModel alloc] initWithString:message error:nil];
    
    switch(model.mediaType) {
        case kAVIMMessageMediaTypeText: {
            [conversation sendMessage:[AVIMTextMessage messageWithText:model.payload attributes:nil] callback:^(BOOL succeeded, NSError * _Nullable error) {
                callback(succeeded, error);
            }];
            break;
        }
        case kAVIMMessageMediaTypeImage: {
            [conversation sendMessage:[AVIMImageMessage messageWithText:nil attachedFilePath:model.payload attributes:nil] callback:^(BOOL succeeded, NSError * _Nullable error) {
                callback(succeeded, error);
            }];
            break;
        }
        case kAVIMMessageMediaTypeAudio: {
            [conversation sendMessage:[AVIMAudioMessage messageWithText:nil attachedFilePath:model.payload attributes:nil] callback:^(BOOL succeeded, NSError * _Nullable error) {
                callback(succeeded, error);
            }];
            break;
        }
        default:break;
    }
}

- (void) findConversationsWithCallback:(NSInteger)limit  callback:(void (^)(NSArray<AVIMConversation *> * , NSError * _Nullable))callback {
    AVIMConversationQuery *query = [self.client conversationQuery];
    query.limit = limit;
    query.option = AVIMConversationQueryOptionWithMessage;
    query.cachePolicy = kAVCachePolicyNetworkElseCache;
    [query findConversationsWithCallback:^(NSArray *objects, NSError *error) {
        callback(objects, error);
    }];
}

- (void) queryMessages: (NSString*)conversationId limit:(NSInteger)limit callback:(void (^)(NSArray<AVIMMessage *> * _Nullable, NSError * _Nullable))callback {
    AVIMConversation* conversation = [self getConversationById: conversationId];
    [conversation queryMessagesWithLimit:limit callback:^(NSArray<AVIMMessage *> * _Nullable messages, NSError * _Nullable error) {
        callback(messages, error);
    }];
}

- (void) updateConversationName: (NSString*)conversationId name:(NSString*)name callback:(void (^)(BOOL, NSError * _Nullable))callback {
    AVIMConversation* conversation = [self getConversationById: conversationId];
    [conversation setObject:name forKey:@"name"];
    [conversation updateWithCallback:^(BOOL succeeded, NSError * _Nullable error) {
        callback(succeeded, error);
    }];
}

#pragma mark - AVIMClientDelegate
- (void)conversation:(AVIMConversation *)conversation membersAdded:(NSArray *)clientIds byClientId:(NSString *)clientId {
    _cachedConversations[conversation.conversationId] = conversation;
    NSLog(@"%@", [NSString stringWithFormat:@"%@ 加入到对话，操作者为：%@",[clientIds objectAtIndex:0],clientId]);
}

- (void)conversation:(AVIMConversation *)conversation didUpdateForKey:(NSString *)key {
    if ([key isEqualToString:@"unreadMessagesCount"]) {
        _cachedConversations[conversation.conversationId] = conversation;
        ConversationModel* model = _convertConverstionToModel(conversation);
        NSDictionary* arguments = @{@"conversation": model.toJSONString};
        [self.channel invokeMethod:@"unreadMessagesCountUpdated" arguments:arguments];
    }
}

- (void)imClientPaused:(AVIMClient *)imClient {
    NSLog(@"imClient Paused");
    NSDictionary* arguments = @{@"status": @"paused"};
    [self.channel invokeMethod:@"onClientStatusChanged" arguments:arguments];
}

- (void)imClientResuming:(AVIMClient *)imClient {
    NSLog(@"imClient Resuming");
    // NSDictionary* arguments = @{@"status": @"resuming"};
    // [self.channel invokeMethod:@"onClientStatusChanged" arguments:arguments];
}

- (void)imClientResumed:(AVIMClient *)imClient {
    NSLog(@"imClient Resumed");
    NSDictionary* arguments = @{@"status": @"resume"};
    [self.channel invokeMethod:@"onClientStatusChanged" arguments:arguments];
}

- (void)imClientClosed:(AVIMClient *)imClient error:(NSError *_Nullable)error {
    if (error != nil) {
        NSLog(@"imClient Closed%@", error.localizedDescription);
    }
    else {
        NSLog(@"imClient Closed");
    }
    NSDictionary* arguments = @{@"status": @"offline"};
    [self.channel invokeMethod:@"onClientStatusChanged" arguments:arguments];
}

- (void)conversation:(AVIMConversation *)conversation didReceiveTypedMessage:(AVIMTypedMessage *)message {
    _cachedConversations[conversation.conversationId] = conversation;
    ConversationModel* conversationModel = _convertConverstionToModel(conversation);
    MessageModel* messageModel = _convertMessageToModel(message);
    NSDictionary* arguments = @{@"conversation": conversationModel.toJSONString, @"message": messageModel.toJSONString};
    [self.channel invokeMethod:@"receiveMessage" arguments:arguments];
}

static FlutterMethodChannel* _channel = nil;
static LeancloudImPlugin* _instance = nil;
+(instancetype) shareInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init] ;
    }) ;
    return _instance ;
}

- (AVIMConversation*) getConversationById:(NSString*) conversationId {
    AVIMConversation* conversation = _cachedConversations[conversationId];
    return conversation;
}

ConversationModel* _convertConverstionToModel(AVIMConversation* conversation) {
    ConversationModel* conversationModel = [[ConversationModel alloc] init];
    conversationModel.unreadMessagesCount = conversation.unreadMessagesCount;
    conversationModel.name = conversation.name;
    conversationModel.creator = conversation.creator;
    conversationModel.lastMessage = _convertMessageToModel(conversation.lastMessage);
    conversationModel.conversationId = conversation.conversationId;
    conversationModel.members = conversation.members;
    conversationModel.updatedAt = [conversation.updateAt timeIntervalSince1970] * 1000;
    conversationModel.lastMessageAt = [conversation.lastMessageAt timeIntervalSince1970] * 1000;
    return conversationModel;
}

MessageModel* _convertMessageToModel(AVIMMessage* message) {
    MessageModel* messageModel = [[MessageModel alloc] init];
    messageModel.messageId = message.messageId;
    messageModel.status = message.status;
    messageModel.conversationId = message.conversationId;
    messageModel.timestamp = message.sendTimestamp;
    messageModel.receiptTimestamp = message.deliveredTimestamp;
    messageModel.clientId = message.clientId;
    messageModel.ioType = message.ioType;
    messageModel.mediaType = message.mediaType;
    messageModel.content = message.content;
    switch (message.mediaType) {
        case kAVIMMessageMediaTypeText:
            messageModel.payload = ((AVIMTextMessage*) message).text;
            break;
        case kAVIMMessageMediaTypeImage:
            messageModel.payload = ((AVIMImageMessage*) message).file.url;
        case kAVIMMessageMediaTypeAudio:
            messageModel.payload = ((AVIMAudioMessage*) message).file.url;
        default:
            break;
    }
    return messageModel;
}
@end
