#import <Flutter/Flutter.h>
#import <AVOSCloudIM/AVOSCloudIM.h>

@interface LeancloudImPlugin : NSObject<FlutterPlugin, AVIMClientDelegate>
+(instancetype) shareInstance;
@property FlutterMethodChannel* channel;
@property NSMutableDictionary* cachedConversations;
@property (strong, nonatomic) AVIMClient* client;

- (void) setApplicationId: (NSString*)appId appKey:(NSString*)appKey;
- (void) initClient: (NSString*)clientId;
- (void) openWithCallback: (void (^)(BOOL, NSError * _Nullable))callback;
- (void) closeWithCallback: (void (^)(BOOL, NSError * _Nullable))callback;
- (void) createConversationWithName: (NSString*)name clientIds:(NSArray<NSString*> *)clientIds callback:(void (^)(AVIMConversation*, NSError * _Nullable))callback;
- (void) sendMessage: (NSString*)conversationId message:(NSString*)message callback:(void (^)(BOOL, NSError * _Nullable))callback;
- (void) findConversationsWithCallback:(NSInteger)limit  callback:(void (^)(NSArray<AVIMConversation *> * , NSError * _Nullable))callback;
- (void) queryMessages: (NSString*)conversationId limit: (NSInteger)limit callback:(void (^)(NSArray<AVIMMessage *> * _Nullable, NSError * _Nullable))callback;
- (void) updateConversationName: (NSString*)conversationId name:(NSString*)name callback:(void (^)(BOOL, NSError * _Nullable))callback;
- (AVIMConversation*) getConversationById:(NSString*) conversationId;
@end
