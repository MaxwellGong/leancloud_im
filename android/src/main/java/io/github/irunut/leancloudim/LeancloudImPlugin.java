package io.github.irunut.leancloudim;

import android.content.Context;
import android.util.Log;

import com.alibaba.fastjson.JSON;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.SaveCallback;
import com.avos.avoscloud.im.v2.AVIMClient;
import com.avos.avoscloud.im.v2.AVIMClientEventHandler;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMConversationEventHandler;
import com.avos.avoscloud.im.v2.AVIMConversationsQuery;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.AVIMMessage;
import com.avos.avoscloud.im.v2.AVIMMessageHandler;
import com.avos.avoscloud.im.v2.AVIMMessageManager;
import com.avos.avoscloud.im.v2.AVIMReservedMessageType;
import com.avos.avoscloud.im.v2.AVIMTypedMessage;
import com.avos.avoscloud.im.v2.callback.AVIMClientCallback;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCallback;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCreatedCallback;
import com.avos.avoscloud.im.v2.callback.AVIMConversationQueryCallback;
import com.avos.avoscloud.im.v2.callback.AVIMMessagesQueryCallback;
import com.avos.avoscloud.im.v2.messages.AVIMAudioMessage;
import com.avos.avoscloud.im.v2.messages.AVIMFileMessage;
import com.avos.avoscloud.im.v2.messages.AVIMImageMessage;
import com.avos.avoscloud.im.v2.messages.AVIMTextMessage;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;
import com.avos.avoscloud.AVOSCloud;
import com.avos.avoscloud.im.v2.messages.AVIMVideoMessage;

/** LeancloudImPlugin */
public class LeancloudImPlugin implements MethodCallHandler {
  private static Context _applicationContext;
  private static MethodChannel channel;

  /** Plugin registration. */
  public static void registerWith(Registrar registrar) {
    channel = new MethodChannel(registrar.messenger(), "leancloud_im");
    channel.setMethodCallHandler(new LeancloudImPlugin());
    _applicationContext = registrar.context().getApplicationContext();
  }

  @Override
  public void onMethodCall(MethodCall call, final Result result) {
    if (call.method.equals("getPlatformVersion")) {
      result.success("Android " + android.os.Build.VERSION.RELEASE);
    } else if (call.method.equals("setApplicationId")) {
      String appId = call.argument("appId");
      String appKey = call.argument("appKey");
      setApplicationId(appId, appKey);
    } else if (call.method.equals("setClient")) {
      String clientId = call.argument("clientId");
      initClient(clientId);
      result.success(true);
    } else if (call.method.equals("open")) {
      client.open(new AVIMClientCallback() {
        @Override
        public void done(AVIMClient client, AVIMException e) {
          if (e == null) {
            result.success(true);
          } else {
            result.error("leancloud_im", e.getLocalizedMessage(), " ");
          }
        }
      });
    } else if (call.method.equals("close")) {
      client.close(new AVIMClientCallback() {
        @Override
        public void done(AVIMClient client, AVIMException e) {
          if (e == null) {
            result.success(true);
          } else {
            result.error("leancloud_im", e.getLocalizedMessage(), " ");
          }
        }
      });
    } else if (call.method.equals("createConversation")) {
      String name = call.argument("name");
      ArrayList<String> clientIds = call.argument("clientIds");
      client.createConversation(clientIds, name, null, false, true, new AVIMConversationCreatedCallback() {
        @Override
        public void done(AVIMConversation conversation, AVIMException e) {
          if (e == null) {
            cachedConversations.put(conversation.getConversationId(), conversation);
            ConversationModel model = convertConversationToModel(conversation);
            result.success(JSON.toJSONString(model));
          } else {
            result.error("leancloud_im", e.getLocalizedMessage(), " ");
          }
        }
      });
    } else if (call.method.equals("sendMessage")) {
      String conversationId = call.argument("conversationId");
      String message = call.argument("message");
      try {
        sendMessage(conversationId, message, new AVIMConversationCallback() {
          @Override
          public void done(AVIMException e) {
            if (e == null) {
              result.success(true);
            } else {
              result.error("leancloud_im", e.getLocalizedMessage(), " ");
            }
          }
        });
      } catch (Exception e) {
        Log.e("leancloud_im", e.getLocalizedMessage());
      }
    } else if (call.method.equals("queryConversationList")) {
      Integer limit = call.argument("limit");
      AVIMConversationsQuery query = client.getConversationsQuery();
      query.limit(limit);
      query.setWithLastMessagesRefreshed(true);
      query.setQueryPolicy(AVQuery.CachePolicy.NETWORK_ELSE_CACHE);
      query.findInBackground(new AVIMConversationQueryCallback() {
        @Override
        public void done(List<AVIMConversation> convs, AVIMException e) {
          if (e == null) {
            ArrayList<String> conversationsJson = new ArrayList<>();
            for (AVIMConversation object : convs) {
              cachedConversations.put(object.getConversationId(), object);
              ConversationModel model = convertConversationToModel(object);
              conversationsJson.add(0, JSON.toJSONString(model));
            }
            result.success(conversationsJson);
          } else {
            result.error("leancloud_im", e.getLocalizedMessage(), " ");
          }
        }
      });
    } else if (call.method.equals("conversationRead")) {
      String conversationId = call.argument("conversationId");
      AVIMConversation conversation = cachedConversations.get(conversationId);
      conversation.read();
    } else if (call.method.equals("queryMessages")) {
      Integer limit = call.argument("limit");
      String conversationId = call.argument("conversationId");
      AVIMConversation conversation = cachedConversations.get(conversationId);
      conversation.queryMessages(limit, new AVIMMessagesQueryCallback() {
        @Override
        public void done(List<AVIMMessage> messages, AVIMException e) {
          if (e == null) {
            ArrayList<String> messagesJson = new ArrayList<>();
            for (AVIMMessage object : messages) {
              MessageModel model = convertMessageToModel(object);
              messagesJson.add(JSON.toJSONString(model));
            }
            result.success(messagesJson);
          } else {
            result.error("leancloud_im", e.getLocalizedMessage(), " ");
          }
        }
      });
    } else if (call.method.equals("updateConversationName")) {
      String conversationId = call.argument("conversationId");
      String name = call.argument("name");
      AVIMConversation conversation = cachedConversations.get(conversationId);
      conversation.setName(name);
      conversation.updateInfoInBackground(new AVIMConversationCallback() {
        @Override
        public void done(AVIMException e) {
          if (e == null) {
            result.success(true);
          } else {
            result.error("leancloud_im", e.getLocalizedMessage(), " ");
          }
        }
      });
    } else {
      result.notImplemented();
    }
  }

  private void setApplicationId(String appId, String appKey) {
    AVOSCloud.useAVCloudUS();
    AVOSCloud.initialize(_applicationContext, appId, appKey);
    AVOSCloud.setDebugLogEnabled(true);
    AVIMClient.setUnreadNotificationEnabled(true);
    // 自定义实现的 AVIMClientEventHandler 需要注册到 SDK 后，SDK 才会通过回调 onClientOffline 来通知开发者
    AVIMClient.setClientEventHandler(new AVImClientManager());
    registerMessageHandler();
  }

  private static AVIMClient client;
  private HashMap<String, AVIMConversation> cachedConversations = new HashMap<String, AVIMConversation>();

  private void initClient(String clientId) {
    client = AVIMClient.getInstance(clientId);
  };

  private void sendMessage(String conversationId, String message, final AVIMConversationCallback callback) {
    final AVIMConversation conversation = cachedConversations.get(conversationId);
    MessageModel model = JSON.parseObject(message, MessageModel.class);
    if (model.getMediaType() == AVIMReservedMessageType.TextMessageType.getType()) {
      AVIMTextMessage msg = new AVIMTextMessage();
      msg.setText(model.getPayload());
      conversation.sendMessage(msg, callback);
    } else if (model.getMediaType() == AVIMReservedMessageType.ImageMessageType.getType()) {
      try {
        AVIMImageMessage msg = new AVIMImageMessage(model.getPayload());
        conversation.sendMessage(msg, callback);
      } catch (Exception e) {
        Log.e("leancloud_im", e.getLocalizedMessage());
      }
    } else if (model.getMediaType() == AVIMReservedMessageType.AudioMessageType.getType()) {
      try {
        final AVFile file = AVFile.withAbsoluteLocalPath("audio.m4a", model.getPayload());
        file.saveInBackground(new SaveCallback() {
          @Override
          public void done(AVException e) {
            if (e == null) {
              AVFile newFile = new AVFile(file.getName(), file.getUrl(), file.getMetaData());
              // 返回一个唯一的 Url 地址
              AVIMAudioMessage msg = new AVIMAudioMessage(newFile);

              conversation.sendMessage(msg, callback);
            }
          }
        });

      } catch (Exception e) {
        Log.e("leancloud_im", e.getLocalizedMessage());
      }
    }

  }

  private void registerMessageHandler() {
    AVIMMessageManager.registerMessageHandler(AVIMMessage.class, new AVIMMessageHandler() {
      @Override
      public void onMessage(AVIMMessage message, AVIMConversation conversation, AVIMClient client) {
        cachedConversations.put(conversation.getConversationId(), conversation);
        ConversationModel conversationModel = convertConversationToModel(conversation);
        MessageModel messageModel = convertMessageToModel(message);
        HashMap<String, String> para = new HashMap<>();
        para.put("conversation", JSON.toJSONString(conversationModel));
        para.put("message", JSON.toJSONString(messageModel));
        channel.invokeMethod("receiveMessage", para);
      }

      @Override
      public void onMessageReceipt(AVIMMessage message, AVIMConversation conversation, AVIMClient client) {
      }
    });

    AVIMMessageManager.setConversationEventHandler(new AVIMConversationEventHandler() {
      @Override
      public void onUnreadMessagesCountUpdated(AVIMClient client, AVIMConversation conversation) {
        ConversationModel model = convertConversationToModel(conversation);
        HashMap<String, String> para = new HashMap<>();
        para.put("conversation", JSON.toJSONString(model));
        channel.invokeMethod("unreadMessagesCountUpdated", para);
      }

      @Override
      public void onMemberLeft(AVIMClient client, AVIMConversation conversation, List<String> members,
          String kickedBy) {

      }

      @Override
      public void onMemberJoined(AVIMClient client, AVIMConversation conversation, List<String> members,
          String invitedBy) {

      }

      @Override
      public void onKicked(AVIMClient client, AVIMConversation conversation, String kickedBy) {

      }

      @Override
      public void onInvited(AVIMClient client, AVIMConversation conversation, String operator) {

      }
    });

  }

  private ConversationModel convertConversationToModel(AVIMConversation conversation) {
    ConversationModel conversationModel = new ConversationModel();
    conversationModel.setUnreadMessagesCount(conversation.getUnreadMessagesCount());
    conversationModel.setName(conversation.getName());
    conversationModel.setCreator(conversation.getCreator());
    conversationModel.setLastMessage(convertMessageToModel(conversation.getLastMessage()));
    conversationModel.setConversationId(conversation.getConversationId());
    conversationModel.setMembers(conversation.getMembers());
    conversationModel.setUpdatedAt(conversation.getUpdatedAt().getTime());
    if (conversation.getLastMessageAt() != null) {
      conversationModel.setLastMessageAt(conversation.getLastMessageAt().getTime());
    }
    return conversationModel;
  }

  private MessageModel convertMessageToModel(AVIMMessage message) {
    if (message == null) {
      return null;
    }
    MessageModel messageModel = new MessageModel();
    messageModel.setStatus(message.getMessageStatus().getStatusCode());
    messageModel.setConversationId(message.getConversationId());
    messageModel.setTimestamp(message.getTimestamp());
    messageModel.setReceiptTimestamp(message.getReceiptTimestamp());
    messageModel.setMessageId(message.getMessageId());
    messageModel.setClientId(message.getFrom());
    if (message.getFrom().equals(client.getClientId())) {
      messageModel.setIoType(2);
    } else {
      messageModel.setIoType(1);
    }
    // messageModel.setIoType(message.getMessageIOType().getIOType());
    if (null != message && message instanceof AVIMTypedMessage) {
      AVIMTypedMessage typedMessage = (AVIMTypedMessage) message;
      messageModel.setMediaType(typedMessage.getMessageType());
      int mediaType = typedMessage.getMessageType();
      if (mediaType == AVIMReservedMessageType.TextMessageType.getType()) {
        AVIMTextMessage textMsg = (AVIMTextMessage) typedMessage;
        messageModel.setPayload(textMsg.getText());
      } else if (mediaType == AVIMReservedMessageType.ImageMessageType.getType()) {
        AVIMImageMessage imageMsg = (AVIMImageMessage) message;
        messageModel.setPayload(imageMsg.getFileUrl());
      } else if (mediaType == AVIMReservedMessageType.AudioMessageType.getType()) {
        AVIMAudioMessage audioMsg = (AVIMAudioMessage) message;
        messageModel.setPayload(audioMsg.getFileUrl());
      } else if (mediaType == AVIMReservedMessageType.VideoMessageType.getType()) {
        AVIMVideoMessage videoMsg = (AVIMVideoMessage) message;

      }
    } else {
      messageModel.setMediaType(0);
    }

    return messageModel;
  }

  class AVImClientManager extends AVIMClientEventHandler {
    @Override
    public void onClientOffline(AVIMClient avimClient, int i) {
      if (i == 4111) {
        // 适当地弹出友好提示，告知当前用户的 Client Id 在其他设备上登陆了
      }
      HashMap<String, String> para = new HashMap<>();
      para.put("status", "offline");
      channel.invokeMethod("onClientStatusChanged", para);
    }

    @Override
    public void onConnectionPaused(AVIMClient client) {
      HashMap<String, String> para = new HashMap<>();
      para.put("status", "paused");
      channel.invokeMethod("onClientStatusChanged", para);
    }

    @Override
    public void onConnectionResume(AVIMClient client) {
      HashMap<String, String> para = new HashMap<>();
      para.put("status", "resume");
      channel.invokeMethod("onClientStatusChanged", para);
    }
  }
}
