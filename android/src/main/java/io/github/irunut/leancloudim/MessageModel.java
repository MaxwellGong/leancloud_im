package io.github.irunut.leancloudim;

import com.avos.avoscloud.im.v2.AVIMMessage;

public class MessageModel {
    private int status;
    private String conversationId;
    private long timestamp;
    private long receiptTimestamp;
    private String clientId;
    private String content;

    public MessageModel() {
    }

    public MessageModel(int status, String conversationId, long timestamp, long receiptTimestamp, String clientId, String content, String messageId, String payload, int ioType, int mediaType) {
        this.status = status;
        this.conversationId = conversationId;
        this.timestamp = timestamp;
        this.receiptTimestamp = receiptTimestamp;
        this.clientId = clientId;
        this.content = content;
        this.messageId = messageId;
        this.payload = payload;
        this.ioType = ioType;
        this.mediaType = mediaType;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getReceiptTimestamp() {
        return receiptTimestamp;
    }

    public void setReceiptTimestamp(long receiptTimestamp) {
        this.receiptTimestamp = receiptTimestamp;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public int getIoType() {
        return ioType;
    }

    public void setIoType(int ioType) {
        this.ioType = ioType;
    }

    public int getMediaType() {
        return mediaType;
    }

    public void setMediaType(int mediaType) {
        this.mediaType = mediaType;
    }

    private String messageId;
    private String payload;
    private int ioType;
    private int mediaType;
}
