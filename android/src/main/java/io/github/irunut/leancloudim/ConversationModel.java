package io.github.irunut.leancloudim;

import java.util.List;

public class ConversationModel {
    private int unreadMessagesCount;
    private String name;
    private String creator;
    private MessageModel lastMessage;
    private String conversationId;
    private List<String> members;
    private long updatedAt;

    public ConversationModel() {
    }

    public ConversationModel(int unreadMessagesCount, String name, String creator, MessageModel lastMessage, String conversationId, List<String> members, long updatedAt, long lastMessageAt) {
        this.unreadMessagesCount = unreadMessagesCount;
        this.name = name;
        this.creator = creator;
        this.lastMessage = lastMessage;
        this.conversationId = conversationId;
        this.members = members;
        this.updatedAt = updatedAt;
        this.lastMessageAt = lastMessageAt;
    }

    public int getUnreadMessagesCount() {
        return unreadMessagesCount;
    }

    public void setUnreadMessagesCount(int unreadMessagesCount) {
        this.unreadMessagesCount = unreadMessagesCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public MessageModel getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(MessageModel lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public long getLastMessageAt() {
        return lastMessageAt;
    }

    public void setLastMessageAt(long lastMessageAt) {
        this.lastMessageAt = lastMessageAt;
    }

    private long lastMessageAt;
}
